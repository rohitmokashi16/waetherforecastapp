Feature: Weather Forecast Controller

  Scenario: Client makes call to GET /forecast with zipcode as Query Parameter
    When the client calls /forecast with "27601" zipcode as query parameter
    Then the client receives response with status code of 200

  Scenario: Client makes call to GET /forecast without zipcode
    When the client calls /forecast with "" zipcode as query parameter
    Then the client receives response with status code of 400

  Scenario: Client makes call to GET /forecast with invalid zipcode
    When the client calls /forecast with "wiuqwhd" zipcode as query parameter
    Then the client receives response with status code of 400
