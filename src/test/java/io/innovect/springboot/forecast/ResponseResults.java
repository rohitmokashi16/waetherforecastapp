package io.innovect.springboot.forecast;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.springframework.http.client.ClientHttpResponse;

public class ResponseResults {
	
	private final ClientHttpResponse apiResponse;
	private final String body;

	@SuppressWarnings("deprecation")
	public ResponseResults(final ClientHttpResponse response) throws IOException {
		this.apiResponse = response;
		final InputStream bodyInputStream = response.getBody();
		final StringWriter stringWriter = new StringWriter();
		IOUtils.copy(bodyInputStream, stringWriter);
		this.body = stringWriter.toString();
	}

	public ClientHttpResponse getTheResponse() {
		return apiResponse;
	}

	public String getBody() {
		return body;
	}
}
