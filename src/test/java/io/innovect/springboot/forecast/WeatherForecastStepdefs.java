package io.innovect.springboot.forecast;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.innovect.springboot.mapper.ForecastResponse;

public class WeatherForecastStepdefs extends SpringIntegrationTest {

	ResponseEntity<ForecastResponse> response;

	@When("the client calls \\/forecast with {string} zipcode as query parameter")
	public void the_client_calls_forecast_with_zipcode_as_query_parameter(String zipcode) throws Throwable {
		executeGet(super.DEFAULT_URL + "/forecast" + "?" + "zipcode" + "=" + zipcode);
	}

	@Then("the client receives response with status code of {int}")
	public void the_client_receives_response_with_status_code_of(Integer statusCode) throws Throwable {
		HttpStatus currentStatusCode = latestResponse.getTheResponse().getStatusCode();
		assertThat("status code is incorrect : " + latestResponse.getBody(), currentStatusCode.value(), is(statusCode));
	}
}
