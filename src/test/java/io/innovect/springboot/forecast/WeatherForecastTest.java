package io.innovect.springboot.forecast;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", extraGlue = "io.innovect.springboot.forecast")
public class WeatherForecastTest {

}
