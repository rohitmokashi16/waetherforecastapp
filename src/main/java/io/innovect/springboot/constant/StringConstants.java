package io.innovect.springboot.constant;

public class StringConstants {
	
	public static final String BACK_SLASH = "/";
	
	public static final String QUESTION_MARK = "?";
	
	public static final String EQUALS = "=";
	
	public static final String KEY = "key";
	
	public static final String AMPERSAND = "&";
	
	public static final String HOURS = "hours";
	
	public static final String HOURS_VALUE = "48";
	
	public static final String POSTAL_CODE = "postal_code";
	
	public static final String COUNTRY = "US";
}
