package io.innovect.springboot.mapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class HourDetails {

	private String timestamp_local;
	private String timestamp_utc;
	private Integer ts;
	private String datetime;
	private Float wind_gust_spd;
	private Float wind_spd;
	private Float wind_dir;
	private String wind_cdir;
	private String wind_cdir_full;
	private Float temp;
	private Float app_temp;
	private Float pop;
	private Float precip;
	private Float snow;
	private Float snow_depth;
	private Float slp;
	private Float pres;
	private Float dewpt;
	private Float rh;
	private Weather weather;
	private String pod;
	private Float clouds_low;
	private Float clouds_mid;
	private Float clouds_hi;
	private Float clouds;
	private Float vis;
	private Float dhi;
	private Float dni;
	private Float ghi;
	private Float solar_rad;
	private Float uv;
	private Float ozone;

	@JsonSerialize()
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Weather {
		private String icon;
		private String code;
		private String description;

		public Weather() {
			super();
		}

		public Weather(String icon, String code, String description) {
			super();
			this.icon = icon;
			this.code = code;
			this.description = description;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return "Weather [icon=" + icon + ", code=" + code + ", description=" + description + "]";
		}

	}

	public HourDetails() {
		super();
	}

	public HourDetails(String timestamp_local, String timestamp_utc, Integer ts, String datetime, Float wind_gust_spd,
			Float wind_spd, Float wind_dir, String wind_cdir, String wind_cdir_full, Float temp, Float app_temp,
			Float pop, Float precip, Float snow, Float snow_depth, Float slp, Float pres, Float dewpt, Float rh,
			Weather weather, String pod, Float clouds_low, Float clouds_mid, Float clouds_hi, Float clouds, Float vis,
			Float dhi, Float dni, Float ghi, Float solar_rad, Float uv, Float ozone) {
		super();
		this.timestamp_local = timestamp_local;
		this.timestamp_utc = timestamp_utc;
		this.ts = ts;
		this.datetime = datetime;
		this.wind_gust_spd = wind_gust_spd;
		this.wind_spd = wind_spd;
		this.wind_dir = wind_dir;
		this.wind_cdir = wind_cdir;
		this.wind_cdir_full = wind_cdir_full;
		this.temp = temp;
		this.app_temp = app_temp;
		this.pop = pop;
		this.precip = precip;
		this.snow = snow;
		this.snow_depth = snow_depth;
		this.slp = slp;
		this.pres = pres;
		this.dewpt = dewpt;
		this.rh = rh;
		this.weather = weather;
		this.pod = pod;
		this.clouds_low = clouds_low;
		this.clouds_mid = clouds_mid;
		this.clouds_hi = clouds_hi;
		this.clouds = clouds;
		this.vis = vis;
		this.dhi = dhi;
		this.dni = dni;
		this.ghi = ghi;
		this.solar_rad = solar_rad;
		this.uv = uv;
		this.ozone = ozone;
	}

	public String getTimestamp_local() {
		return timestamp_local;
	}

	public void setTimestamp_local(String timestamp_local) {
		this.timestamp_local = timestamp_local;
	}

	public String getTimestamp_utc() {
		return timestamp_utc;
	}

	public void setTimestamp_utc(String timestamp_utc) {
		this.timestamp_utc = timestamp_utc;
	}

	public Integer getTs() {
		return ts;
	}

	public void setTs(Integer ts) {
		this.ts = ts;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public Float getWind_gust_spd() {
		return wind_gust_spd;
	}

	public void setWind_gust_spd(Float wind_gust_spd) {
		this.wind_gust_spd = wind_gust_spd;
	}

	public Float getWind_spd() {
		return wind_spd;
	}

	public void setWind_spd(Float wind_spd) {
		this.wind_spd = wind_spd;
	}

	public Float getWind_dir() {
		return wind_dir;
	}

	public void setWind_dir(Float wind_dir) {
		this.wind_dir = wind_dir;
	}

	public String getWind_cdir() {
		return wind_cdir;
	}

	public void setWind_cdir(String wind_cdir) {
		this.wind_cdir = wind_cdir;
	}

	public String getWind_cdir_full() {
		return wind_cdir_full;
	}

	public void setWind_cdir_full(String wind_cdir_full) {
		this.wind_cdir_full = wind_cdir_full;
	}

	public Float getTemp() {
		return temp;
	}

	public void setTemp(Float temp) {
		this.temp = temp;
	}

	public Float getApp_temp() {
		return app_temp;
	}

	public void setApp_temp(Float app_temp) {
		this.app_temp = app_temp;
	}

	public Float getPop() {
		return pop;
	}

	public void setPop(Float pop) {
		this.pop = pop;
	}

	public Float getPrecip() {
		return precip;
	}

	public void setPrecip(Float precip) {
		this.precip = precip;
	}

	public Float getSnow() {
		return snow;
	}

	public void setSnow(Float snow) {
		this.snow = snow;
	}

	public Float getSnow_depth() {
		return snow_depth;
	}

	public void setSnow_depth(Float snow_depth) {
		this.snow_depth = snow_depth;
	}

	public Float getSlp() {
		return slp;
	}

	public void setSlp(Float slp) {
		this.slp = slp;
	}

	public Float getPres() {
		return pres;
	}

	public void setPres(Float pres) {
		this.pres = pres;
	}

	public Float getDewpt() {
		return dewpt;
	}

	public void setDewpt(Float dewpt) {
		this.dewpt = dewpt;
	}

	public Float getRh() {
		return rh;
	}

	public void setRh(Float rh) {
		this.rh = rh;
	}

	public Weather getWeather() {
		return weather;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public Float getClouds_low() {
		return clouds_low;
	}

	public void setClouds_low(Float clouds_low) {
		this.clouds_low = clouds_low;
	}

	public Float getClouds_mid() {
		return clouds_mid;
	}

	public void setClouds_mid(Float clouds_mid) {
		this.clouds_mid = clouds_mid;
	}

	public Float getClouds_hi() {
		return clouds_hi;
	}

	public void setClouds_hi(Float clouds_hi) {
		this.clouds_hi = clouds_hi;
	}

	public Float getClouds() {
		return clouds;
	}

	public void setClouds(Float clouds) {
		this.clouds = clouds;
	}

	public Float getVis() {
		return vis;
	}

	public void setVis(Float vis) {
		this.vis = vis;
	}

	public Float getDhi() {
		return dhi;
	}

	public void setDhi(Float dhi) {
		this.dhi = dhi;
	}

	public Float getDni() {
		return dni;
	}

	public void setDni(Float dni) {
		this.dni = dni;
	}

	public Float getGhi() {
		return ghi;
	}

	public void setGhi(Float ghi) {
		this.ghi = ghi;
	}

	public Float getSolar_rad() {
		return solar_rad;
	}

	public void setSolar_rad(Float solar_rad) {
		this.solar_rad = solar_rad;
	}

	public Float getUv() {
		return uv;
	}

	public void setUv(Float uv) {
		this.uv = uv;
	}

	public Float getOzone() {
		return ozone;
	}

	public void setOzone(Float ozone) {
		this.ozone = ozone;
	}

	@Override
	public String toString() {
		return "HourDetails [timestamp_local=" + timestamp_local + ", timestamp_utc=" + timestamp_utc + ", ts=" + ts
				+ ", datetime=" + datetime + ", wind_gust_spd=" + wind_gust_spd + ", wind_spd=" + wind_spd
				+ ", wind_dir=" + wind_dir + ", wind_cdir=" + wind_cdir + ", wind_cdir_full=" + wind_cdir_full
				+ ", temp=" + temp + ", app_temp=" + app_temp + ", pop=" + pop + ", precip=" + precip + ", snow=" + snow
				+ ", snow_depth=" + snow_depth + ", slp=" + slp + ", pres=" + pres + ", dewpt=" + dewpt + ", rh=" + rh
				+ ", weather=" + weather + ", pod=" + pod + ", clouds_low=" + clouds_low + ", clouds_mid=" + clouds_mid
				+ ", clouds_hi=" + clouds_hi + ", clouds=" + clouds + ", vis=" + vis + ", dhi=" + dhi + ", dni=" + dni
				+ ", ghi=" + ghi + ", solar_rad=" + solar_rad + ", uv=" + uv + ", ozone=" + ozone + "]";
	}

}
