package io.innovect.springboot.mapper;

import java.util.List;

public class ForecastResponse {

	private String city_name;
	private String country_code;
	private String state_code;
	private String timezone;
	private Float lat;
	private Float lon;
	private List<HourDetails> data;

	public ForecastResponse() {
		super();
	}

	public ForecastResponse(String city_name, String country_code, String state_code, String timezone, Float lat,
			Float lon, List<HourDetails> data) {
		super();
		this.city_name = city_name;
		this.country_code = country_code;
		this.state_code = state_code;
		this.timezone = timezone;
		this.lat = lat;
		this.lon = lon;
		this.data = data;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getState_code() {
		return state_code;
	}

	public void setState_code(String state_code) {
		this.state_code = state_code;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLon() {
		return lon;
	}

	public void setLon(Float lon) {
		this.lon = lon;
	}

	public List<HourDetails> getData() {
		return data;
	}

	public void setData(List<HourDetails> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ForecastResponse [city_name=" + city_name + ", country_code=" + country_code + ", state_code="
				+ state_code + ", timezone=" + timezone + ", lat=" + lat + ", lon=" + lon + ", data=" + data + "]";
	}

}
