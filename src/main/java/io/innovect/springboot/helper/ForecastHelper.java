package io.innovect.springboot.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.innovect.springboot.constant.StringConstants;
import io.innovect.springboot.mapper.HourDetails;

@Component
public class ForecastHelper {

	@Value("${api.key}")
	private String apiKey;

	@Value("${proxy}")
	private String proxy;

	@Value("${domain}")
	private String domain;

	@Value("${api.version}")
	private String apiVersion;

	@Value("${resource}")
	private String resource;

	@Value("${forecast.type}")
	private String forecastType;

	public String getBaseUrl() {
		return proxy + domain + StringConstants.BACK_SLASH + apiVersion + StringConstants.BACK_SLASH + resource
				+ StringConstants.BACK_SLASH + forecastType + StringConstants.QUESTION_MARK + StringConstants.KEY
				+ StringConstants.EQUALS + apiKey + StringConstants.AMPERSAND + StringConstants.HOURS
				+ StringConstants.HOURS_VALUE + StringConstants.AMPERSAND + StringConstants.POSTAL_CODE
				+ StringConstants.EQUALS;
	}

	public List<HourDetails> filterResultByDate(List<HourDetails> hourDetails) {
		DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
		DateTime tomorrowDateTime = new DateTime().plusDays(1);
		List<HourDetails> filteredHourDetails = new ArrayList<HourDetails>();
		if (hourDetails != null) {
			filteredHourDetails = hourDetails.stream().filter(hourDetail -> dateTimeComparator.compare(tomorrowDateTime,
					new DateTime(hourDetail.getTimestamp_local())) == 0).collect(Collectors.toList());
		}
		return filteredHourDetails;
	}

}
