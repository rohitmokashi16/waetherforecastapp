package io.innovect.springboot.forecast;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.innovect.springboot.mapper.ForecastResponse;

@RestController
public class ForecastController {

	@Autowired
	private ForecastServcie forecastServcie;

	/**
	 * 
	 * @param request HttpServletRequest
	 * @param zipcode String zipcode value query parameter
	 * @return ResponseEntity<ForecastResponse> object if success else BAD_REQUEST
	 */
	@GetMapping("/forecast")
	public ResponseEntity<ForecastResponse> getWeatherForecast(HttpServletRequest request,
			@RequestParam("zipcode") String zipcode) {
		if (zipcode == null || zipcode.isEmpty()) {
			return new ResponseEntity<ForecastResponse>(new ForecastResponse(), HttpStatus.BAD_REQUEST);
		}
		return forecastServcie.getWeatherForecastForZipcode(zipcode);
	}
}
