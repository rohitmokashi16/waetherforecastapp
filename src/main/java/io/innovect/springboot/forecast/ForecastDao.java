package io.innovect.springboot.forecast;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import io.innovect.springboot.mapper.ForecastResponse;

@Component
public class ForecastDao {

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 
	 * @param url String URL value
	 * @param zipcode String zipcode value
	 * @return ForecastResponse object
	 */
	public ForecastResponse fetchWeatherForecast(String url, String zipcode) {
		ForecastResponse forecastResponse = new ForecastResponse();
		try {
			forecastResponse = restTemplate.getForObject(url + zipcode + ",US", ForecastResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return forecastResponse;
	}
}
