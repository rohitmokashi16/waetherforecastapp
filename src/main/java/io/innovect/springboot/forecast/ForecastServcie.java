package io.innovect.springboot.forecast;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import io.innovect.springboot.helper.ForecastHelper;
import io.innovect.springboot.mapper.ForecastResponse;
import io.innovect.springboot.mapper.HourDetails;

@Service
public class ForecastServcie {

	private String regex = "^[0-9]{5}(?:-[0-9]{4})?$";

	private Pattern pattern = Pattern.compile(regex);

	@Autowired
	private ForecastHelper forecastHelper;

	@Autowired
	private ForecastDao forecastDao;

	/**
	 * 
	 * @param zipcode String zipcode value
	 * @return ResponseEntity<ForecastResponse> object if success else BAD_REQUEST
	 */
	public ResponseEntity<ForecastResponse> getWeatherForecastForZipcode(String zipcode) {

		Matcher matcher = pattern.matcher(zipcode);
		if (matcher.matches()) {
			ForecastResponse forecastResponse = forecastDao.fetchWeatherForecast(forecastHelper.getBaseUrl(), zipcode);
			List<HourDetails> filteredHourDetails = forecastHelper.filterResultByDate(forecastResponse.getData());
			forecastResponse.setData(filteredHourDetails);
			return new ResponseEntity<ForecastResponse>(forecastResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<ForecastResponse>(new ForecastResponse(), HttpStatus.BAD_REQUEST);
		}
	}
}
